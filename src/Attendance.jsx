import React, {useState, useEffect} from "react";

const Attendance=()=>{
    const [id, setId] = useState();

    //read data from server in this case as simple as possible
    useEffect(()=>{
        fetch("https://tka-attendance.herokuapp.com/test")
        .then(response=>response.json())
        .then(data=>{
            setId(data.id);
        })
    },[]);

    return(
        <div>
            <h1>Attendance demo</h1>
            Id was {id}
        </div>
    )
}

export default  Attendance;