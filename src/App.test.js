import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Attendance demo', () => {
  render(<App />);
  const linkElement = screen.getByText(/Attendance demo/i);
  expect(linkElement).toBeInTheDocument();
});
